import Home from "./components/Home/Home"
import Header from "./components/Header/Header"
import FlightDetail from "./components/FlightDetail/FlightDetail";
import PageNotFound from "./components/PageNotFound/PageNotFound"
import Footer from "./components/Footer/Footer"

import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
function App() {
  return (
    <div className='app'>
      <Router>
        <Header></Header>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/flight" element={<FlightDetail />} />
          <Route path="*" element={<PageNotFound />} />
        </Routes>
        <Footer />

      </Router>
    </div>
  );
}

export default App;
